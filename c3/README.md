# Chapter 3: Building a HTML printer library

## Learning to build HTML content

The following function wrapHtml takes a content string and appends and prepends 
the html, and body tags, then it returns the newly "tagged" content string.

`wrapHtml content = "<html><body>" <> content <> "</body></html>"`

operators (like `<>`) are infix functions. They take two arguments - one from each side.
There is order of opertations. `<>` has right fixity. For example:

`"<html><body>" <> content <> "</body></html>"`

is viewed as:

`"<html><body>" <> (content <> "</body></html>")`

we can fix issues with mixing multiple operators with the same precedence, 
but different fixity by appropriately adding in parenthesis.

## Adding type signatures

Haskell is statically typed. Haskell is type inferred. However, specifying types
is useful. 

the double-colon (`::`) is used to specify the type of names.

Here are a few examples of types:
- Int
- String
- `Bool`
- `()` - The type of the expression `()`, also called unit
- `a -> b` - The type of a function from an expression of type `a` 
to an expression of type `b`
- `IO ()`

Example on specifying a type:

`title_ :: String -> String`

Haskell handles multiple arguments in an interesting way. Because all functions 
take exactly one argument as an input and return one value as an output. 
A function with _two_ inputs would look like:

`Func :: String -> String -> String`

Haskell parses it as:

`Func :: String -> (String -> String)`

Or a more practical example:
```
addNum :: Int -> Int -> Int
addNum numOne numTwo = numOne + numTwo
```

addNum numOne is also a function... One that takes numTwo as input and returns numOne. 
So we could make function that addsOne, using the addNum function above, by doing:

```
addOne :: Int -> Int
addOne = addNum 1
```

We can also define anonymous functions or lambda functions. 

`\<argument> -> <expression>`

`\` marks the head of the lambda function, and the arrow `->` marks the body. 
We can even chain lambda functions.

`three = (\num1 -> \num2 -> num1 + num2) 1 2`

This can be translated to addNum:

```
addNum :: Int -> (Int -> Int)
addNum = \numOne -> \numTwo -> numOne + numTwo
```

We can write three as:

`three = (\num1 num2 -> num1 + num2) 1 2`

## Safer HTML with types

### `newtype`
`newtype` can be used to define a distinct type for an existing set of values. 
We can represent seconds, minutes, grams using integer values, but we do not 
want to mix grams with seconds.

HTML is going to use text, but we want to distinguish that from strings that are 
not going to be valid HTML.

`newtype <type-name> = <constructor> <existing-type>`

We can define the `Html` type:

`newtype Html = Html String`

### Using `newtypes`

1. case expressions:

    ```
    case <expression> of
        <pattern> -> <expression>
        ...
        <pattern> -> <expression>
    ```
    Here's a practical example:
    ```
    getStructureString :: Structure -> String
    getStructureString struct =
        case struct of
            Structure str -> str
    ```
2. we can also use pattern matching on the args:
    
    `func <pattern> = <expression`
    For example:
    ```
    getStructureString :: Structure -> String
    getStructureString (Structure str) = str
    ```

`newtype` doesn't have performance cost? I feel like I need more information. 
At least, before I take someone's assertion.

### Chaining functions

Here is the compose operator `.`. It is implemented as:

```
(.) :: (b -> c) -> (a -> b) -> a -> c
(.) f g x = f (g x)
```

g(x) happens first then the result gets put into f(). In the case of how we use 
it currently is as follows:

```
p_ :: String -> Structure
p_ Structure . el "p"
```

In our case with the compose operator we have:
```
Structure :: String -> Structure
el "p" :: String -> String
```
Type wise this looks like:
```
b -> c
a -> b
```
We want to make sure we have equivalence.
1. `a ~ String`
2. `b ~ String`
3. `c ~ Structure`

Basically does this fit into our compose operator?:

`(.) :: (b -> c) -> (a -> b) -> (a -> c)`

replacing with our real types we get:

`(.) :: (String -> Structure) -> (String -> String) -> (String -> Structure)` 

So if we compose `Structure` with `el "p"` we get:

`Structure . el "p" :: String -> Structure`

### Using `type`

A `type` definition is similar to `newtype`, but it does not use a constructor. 
For example, we can write:

`type Title = String`

`type` is justs a type name alias. Title and String are interchangeable.

## Modules

Modules are basically libraries.

```
module <module-name>
    ( <export-list>
    , <export.list_2>
    , ...
    )
    where
```

We can omit the export list if we want to be able to use everything in the module. 
We use modules to limit the functions the end user has to interact with. For 
safety!

## Escaping Characters

### Linked Lists
Linked lists are very common. Here some syntactic notes:
1. List types are denoted with brackets and inside them is the type.
    - `[Int]`
    - `[Char]`
    - ... and so on
    - `[a]` - a list of any single type.

2. Empty List == `[]`
3. Prepending an element is done by the `:` operator.
    - `1 : []`
    - `1 : 2 : 3 : []`
4. Can be written as
    - `[1]`
    - `[1, 2, 3]`

### `map`
The type signature is:

`map :: (a -> b) -> [a] -> [b]`

Our practical example with our case we created earlier:

`map escapeChar ['<','h','1','>'] == ["&lt;","h","1","&gt;"]`

### `concat`

`concat :: [[a]] -> [a]`
It flattens a list of lists.

## Internal Modules

Instead of writing the implementation in our `Html` module, we write it in the 
`Html.Internal` module, which will export everything.
Then we will import that module in the `Html` module, and write and explicit 
export list to only export the API we'd like to export.

`Internal` modules are considered unstable and risky to use. A quick search 
yielded 
[this.](https://stackoverflow.com/questions/9190638/how-why-and-when-to-use-the-internal-modules-pattern)

Steps in this process:
- Added `module Html.Internal where` to `./Html/Internal.hs`
- Appended functions in `Html.hs` to `./Html/Internal.hs`
- Added `import Html.Internal` to `Html.hs`

