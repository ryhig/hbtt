-- Html.hs

module Html 
    ( Html
    , Title
    , Structure
    , html_
    , p_
    , code_
    , h1_
    , ol_
    , ul_
    , append_
    , render
    , escape
    )
    where 

import Html.Internal
